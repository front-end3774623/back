import { DataSource } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { config } from 'dotenv';
import { join } from 'path';
import * as process from 'process';

config({
  // ToDo: Need to use dynamic path
  path: join(process.cwd(), '.env.dev'),
});
const configService = new ConfigService();
export default new DataSource({
  type: 'postgres',
  host: configService.get('POSTGRES_HOST'),
  port: +configService.get('POSTGRES_PORT'),
  username: configService.get('POSTGRES_USER'),
  password: configService.get('POSTGRES_PASSWORD'),
  database: configService.get('POSTGRES_DATABASE'),
  migrationsRun: false,
  entities: ['src/**/*.entity.ts'],
  migrations: ['migrations/*.ts'],
  ssl:
    configService.get('ENV') === 'prod'
      ? {
          rejectUnauthorized: false,
        }
      : false,
});
