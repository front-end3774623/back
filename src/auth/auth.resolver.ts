import { Args, Context, Mutation, Resolver } from '@nestjs/graphql';
import { SignUpInput } from './dtos/sign-up.input';
import { AuthService } from './auth.service';
import { UseGuards } from '@nestjs/common';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { LoginInput } from './dtos/login.input';
import { Public } from '../common/decorators/metadata/public.decorator';
import { FastifyReply, FastifyRequest } from 'fastify';
import { LoginResponse } from './dtos/login.response';
import { User } from '../models/users/entities/user.entity';
import { SessionsService } from '../models/sessions/sessions.service';
import { RefreshTokenGuard } from '../models/sessions/guards/refresh-token.guard';
import { CreateUserInput } from '../models/users/dtos/create-user.input';

@Resolver()
export class AuthResolver {
  constructor(
    private authService: AuthService,
    private sessionsService: SessionsService,
  ) {}

  @Mutation(() => LoginResponse)
  @UseGuards(LocalAuthGuard)
  @Public()
  async login(
    @Args('loginInput', { type: () => LoginInput }) { email }: LoginInput,
    @Context()
    { reply, user }: { req: FastifyRequest; reply: FastifyReply; user: User },
  ) {
    const { refreshToken, accessToken } = await this.authService.login(
      user,
      email,
    );

    reply.setCookie('_my_session', refreshToken, {
      httpOnly: true,
      secure: true,
      sameSite: 'none',
      // TODO: Use .env variable
      maxAge: 60 * 15,
    });

    return { user, accessToken };
  }

  @Mutation(() => LoginResponse)
  @UseGuards(RefreshTokenGuard)
  @Public()
  async refresh(
    @Context()
    { req, reply }: { req: FastifyRequest; reply: FastifyReply; user: User },
  ) {
    const oldRefreshToken = req.cookies['_my_session'];

    try {
      const user =
        await this.sessionsService.getUserByRefreshToken(oldRefreshToken);

      const { accessToken, refreshToken } = await this.authService.getTokens(
        user,
        user.auth.email,
      );

      reply.setCookie('_my_session', refreshToken, {
        httpOnly: true,
        secure: true,
        sameSite: 'none',
        // TODO: Use .env variable
        maxAge: 60 * 15,
      });

      return { accessToken, user };
    } catch (error) {
      reply.clearCookie('_my_session');

      throw error;
    }
  }

  @Mutation(() => User)
  @Public()
  async signUp(
    @Args('auth', { type: () => SignUpInput }) auth: SignUpInput,
    @Args('user', { type: () => CreateUserInput }) user: CreateUserInput,
  ) {
    return this.authService.signUp(auth, user);
  }

  @Mutation(() => String)
  async logout(
    @Context()
    { req, reply },
  ) {
    const refreshToken = req.cookies['_my_session'];

    reply.clearCookie('_my_session');

    return this.authService.logout(req.user.id, refreshToken);
  }
}
