import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { FastifyRequest } from 'fastify';
import { UsersService } from '../../models/users/users.service';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(
    private jwtService: JwtService,
    private configService: ConfigService,
    private usersService: UsersService,
    private readonly reflector: Reflector,
  ) {
    super();
  }

  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    return ctx.getContext().req;
  }

  async canActivate(context: ExecutionContext) {
    const isPublic = this.reflector.get<boolean>(
      'isPublic',
      context.getHandler(),
    );

    if (isPublic) {
      return true;
    }

    try {
      const request = this.getRequest(context);

      await this.attachUserToRequestObject(request);
    } catch {
      throw new UnauthorizedException();
    }

    return true;
  }

  private async attachUserToRequestObject(request: any) {
    const isSubscriptionRequest = !!request.connectionParams;

    const token = isSubscriptionRequest
      ? request.connectionParams.accessToken
      : this.extractTokenFromHeader(request);

    const user = await this.extractUserFromToken(token);

    request['user'] = user;
  }

  private extractTokenFromHeader(request: FastifyRequest) {
    const [, token] = request.headers.authorization.split(' ');
    return token;
  }

  private async extractUserFromToken(token: string) {
    const { sub: userId } = await this.jwtService.verifyAsync(token, {
      secret: this.configService.get('JWT_SECRET'),
    });

    return this.usersService.findById(userId);
  }
}
