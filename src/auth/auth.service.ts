import { Injectable } from '@nestjs/common';
import { SignUpInput } from './dtos/sign-up.input';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Auth } from './entities/auth.entity';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../models/users/users.service';
import { SessionsService } from '../models/sessions/sessions.service';
import { User } from '../models/users/entities/user.entity';
import { CreateUserInput } from '../models/users/dtos/create-user.input';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(Auth)
    private authRepository: Repository<Auth>,
    private jwtService: JwtService,
    private usersService: UsersService,
    private sessionsService: SessionsService,
  ) {}

  async validateUser(email: string, password: string) {
    const authInfo = await this.authRepository.findOne({
      where: { email },
      relations: ['user'],
    });

    if (!authInfo) return null;

    const isMatchPassword = await bcrypt.compare(password, authInfo.hash);

    if (!isMatchPassword) return null;
    return authInfo.user;
  }

  async getTokens(user: User, email: string) {
    const accessToken = this.jwtService.sign({
      email,
      sub: user.id,
    });

    const refreshToken = await this.sessionsService.create(user);

    return { accessToken, refreshToken };
  }

  async signUp({ email, password }: SignUpInput, user: CreateUserInput) {
    const hash = await this.hash(password);

    const authInfo = await this.authRepository.save({ email, hash });

    return this.usersService.create(user, authInfo);
  }

  async hash(password: string) {
    const saltRounds = 10;
    const salt = await bcrypt.genSalt(saltRounds);
    return bcrypt.hash(password, salt);
  }

  async login(user: User, email: string) {
    const { accessToken, refreshToken } = await this.getTokens(user, email);

    await this.usersService.update({ id: user.id, isOnline: true });

    return { refreshToken, accessToken };
  }

  async logout(userId: string, refreshToken: string) {
    await this.sessionsService.markRefreshTokenAsUsed(refreshToken);

    await this.usersService.update({ id: userId, isOnline: false });

    return 'Logged out';
  }
}
