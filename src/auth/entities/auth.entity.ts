import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
} from 'typeorm';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { User } from '../../models/users/entities/user.entity';

@Entity({ name: 'auth' })
@ObjectType('Auth')
export class Auth {
  @PrimaryGeneratedColumn('uuid')
  @Field(() => ID)
  id: string;

  @Column({
    length: 100,
    unique: true,
  })
  @Field()
  email: string;

  @Column()
  @Field()
  hash: string;

  @CreateDateColumn()
  @Field()
  created_at: Date;

  @UpdateDateColumn()
  @Field()
  updated_at: Date;

  @OneToOne(() => User, (user) => user.auth)
  user: User;
}
