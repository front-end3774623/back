import { ObjectType, Field } from '@nestjs/graphql';
import { User } from '../../models/users/entities/user.entity';

@ObjectType()
export class LoginResponse {
  @Field()
  accessToken: string;

  @Field()
  user: User;
}
