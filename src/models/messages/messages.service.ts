import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Message } from './entities/message.entity';
import { CreateMessageInput } from './dtos/create-message.input';
import { PubSub } from 'graphql-subscriptions';
import { MESSAGE_CREATED } from './constants';
import { PUB_SUB } from '../../common/constants/injection-token';
import { User } from '../users/entities/user.entity';
import { RoomsService } from '../rooms/rooms.service';

@Injectable()
export class MessagesService {
  constructor(
    @InjectRepository(Message)
    private messagesRepository: Repository<Message>,
    private roomsService: RoomsService,
    @Inject(PUB_SUB) private readonly pubSub: PubSub,
  ) {}

  async findAll(roomId: string, userId: string) {
    const messages = await this.messagesRepository.find({
      where: { room: { id: roomId } },
      relations: { author: true, room: true, users: true },
      order: {
        created_at: 'ASC',
      },
    });

    return messages.map((message) => ({
      ...message,
      isAuthor: message.author.id === userId,
      isRead: !message.users.some((user) => user.id === userId),
    }));
  }

  async create(author: User, createMessageInput: CreateMessageInput) {
    const room = await this.roomsService.findById(
      author.id,
      createMessageInput.roomId,
    );

    const message = this.messagesRepository.create({
      author,
      content: createMessageInput.content,
    });

    message.room = room;
    message.users = room.members.filter((user) => user.id !== author.id);

    const newMessage = await this.messagesRepository.save(message);
    await this.pubSub.publish(MESSAGE_CREATED, {
      messageCreated: newMessage,
    });

    return newMessage;
  }
}
