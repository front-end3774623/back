import {
  Args,
  Context,
  Mutation,
  Query,
  Resolver,
  Subscription,
} from '@nestjs/graphql';
import { Message } from './entities/message.entity';
import { CreateMessageInput } from './dtos/create-message.input';
import { MessagesService } from './messages.service';
import { MESSAGE_CREATED } from './constants';
import { Inject } from '@nestjs/common';
import { PubSub } from 'graphql-subscriptions';
import { MessageResponse } from './dtos/message.response';
import { PUB_SUB } from '../../common/constants/injection-token';
import { User } from '../users/entities/user.entity';

@Resolver()
export class MessagesResolver {
  constructor(
    private messagesService: MessagesService,
    @Inject(PUB_SUB) private readonly pubSub: PubSub,
  ) {}

  @Query(() => [MessageResponse], { name: 'messages' })
  async findAll(
    @Args('roomId', { type: () => String }) roomId: string,
    @Context()
    { req },
  ) {
    return this.messagesService.findAll(roomId, req.user.id);
  }

  @Mutation(() => Message, { name: 'message' })
  async create(
    @Args('createMessageInput', { type: () => CreateMessageInput })
    createMessageInput: CreateMessageInput,
    @Context()
    { req },
  ) {
    return this.messagesService.create(req.user, createMessageInput);
  }

  @Subscription(() => MessageResponse, {
    resolve: (message, _, { req }) => ({
      ...message.messageCreated,
      isRead: message.messageCreated.author.id === req.user.id,
      isAuthor: message.messageCreated.author.id === req.user.id,
    }),
    filter: (message, _, { req }) => {
      const members: User[] = message.messageCreated.room.members;

      return !!members.find((member) => member.id === req.user.id);
    },
  })
  messageCreated() {
    return this.pubSub.asyncIterator(MESSAGE_CREATED);
  }
}
