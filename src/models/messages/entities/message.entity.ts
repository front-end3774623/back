import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';
import { Room } from '../../rooms/entities/room.entity';
import { User } from '../../users/entities/user.entity';

@Entity({ name: 'messages' })
@ObjectType('Message')
export class Message {
  @PrimaryGeneratedColumn('uuid')
  @Field()
  id: string;

  @Field(() => User)
  @ManyToOne(() => User, (user) => user)
  @JoinColumn({ name: 'author_id' })
  author: User;

  @CreateDateColumn()
  @Field()
  created_at: Date;

  @Column('text')
  @Field()
  content: string;

  @ManyToOne(() => Room, (room) => room.messages)
  room: Room;

  @ManyToMany(() => User)
  @JoinTable({
    name: 'messages_users',
  })
  users: User[];
}
