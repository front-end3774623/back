import { Field, ID, InputType } from '@nestjs/graphql';

@InputType()
export class CreateMessageInput {
  @Field(() => ID)
  readonly roomId: string;

  @Field()
  readonly content: string;
}
