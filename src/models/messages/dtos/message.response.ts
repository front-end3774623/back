import { ObjectType, Field, ID } from '@nestjs/graphql';
import { Room } from '../../rooms/entities/room.entity';
import { User } from '../../users/entities/user.entity';

@ObjectType()
export class MessageResponse {
  @Field(() => ID)
  readonly id: string;

  @Field()
  readonly content: string;

  @Field()
  readonly isAuthor: boolean;

  @Field()
  readonly author: User;

  @Field()
  readonly room: Room;

  @Field()
  readonly isRead: boolean;
}
