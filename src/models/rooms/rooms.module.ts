import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Room } from './entities/room.entity';
import { RoomsService } from './rooms.service';
import { UsersModule } from '../users/users.module';
import { RoomsResolver } from './rooms.resolver';
import { RoomUser } from './entities/room-user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Room, RoomUser]), UsersModule],
  providers: [RoomsResolver, RoomsService],
  exports: [RoomsService],
})
export class RoomsModule {}
