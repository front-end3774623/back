import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Room } from './entities/room.entity';
import { In, Repository } from 'typeorm';
import { UsersService } from '../users/users.service';
import { RoomUser } from './entities/room-user.entity';

@Injectable()
export class RoomsService {
  constructor(
    @InjectRepository(Room) private roomRepository: Repository<Room>,
    @InjectRepository(RoomUser)
    private roomUserRepository: Repository<RoomUser>,
    private usersService: UsersService,
  ) {}

  async findById(userId: string, roomId: string) {
    const room = await this.roomRepository.findOne({
      where: { id: roomId, roomsUsers: { user: { id: userId } } },
      relations: {
        roomsUsers: { user: true },
        messages: { users: true },
      },
    });

    if (!room) {
      throw new NotFoundException();
    }

    return this.roomRepository.findOne({
      where: { id: room.id },
      relations: {
        roomsUsers: { user: true },
        messages: { users: true },
      },
    });
  }

  async findByMemberIds(userIds: string[]) {
    const rooms = await this.roomRepository.find({
      where: { roomsUsers: { user: { id: In(userIds) } } },
      relations: {
        roomsUsers: { user: true },
      },
    });

    return rooms.find((room) => room.roomsUsers.length === userIds.length);
  }

  async findAll(userId: string) {
    const rooms = await this.roomRepository.find({
      where: {
        roomsUsers: {
          user: {
            id: userId,
          },
        },
      },
      relations: {
        roomsUsers: { user: true },
      },
    });

    const roomIds = rooms.map((room) => room.id);

    return this.roomRepository.find({
      where: { id: In(roomIds) },
      relations: {
        roomsUsers: { user: true },
      },
    });
  }

  async create(authorId: string, userIds: string[]) {
    const room = this.roomRepository.create();

    const members = await this.usersService.findByIds([...userIds, authorId]);

    await this.roomRepository.save(room);

    await this.roomUserRepository.insert(
      members.map((member) => ({ room, user: member })),
    );

    return room.id;
  }

  async read(userId: string, roomId: string) {
    const room = await this.findById(userId, roomId);

    room.messages = room.messages.map((message) => {
      message.users = message.users.filter((user) => user.id !== userId);
      return message;
    });

    return this.roomRepository.save(room);
  }

  async startDialog(authorId: string, userId: string) {
    const alreadyCreatedRoom = await this.findByMemberIds([authorId, userId]);

    if (alreadyCreatedRoom) {
      return alreadyCreatedRoom.id;
    }

    return this.create(authorId, [userId]);
  }

  async leave(userId: string, roomId: string) {
    await this.roomUserRepository.softDelete({
      user: { id: userId },
      room: { id: roomId },
    });

    return roomId;
  }
}
