import { Args, Context, Mutation, Query, Resolver } from '@nestjs/graphql';
import { RoomsService } from './rooms.service';
import { Room } from './entities/room.entity';
import { CreateRoomInput } from './dtos/create-room.input';

@Resolver()
export class RoomsResolver {
  constructor(private roomsService: RoomsService) {}

  @Query(() => Room, { name: 'room' })
  async find(
    @Args('roomId', { type: () => String }) roomId: string,
    @Context()
    { req },
  ) {
    return this.roomsService.findById(req.user.id, roomId);
  }

  @Query(() => [Room], { name: 'rooms' })
  async findAll(
    @Context()
    { req },
  ) {
    return this.roomsService.findAll(req.user.id);
  }

  @Mutation(() => String, { name: 'createRoom' })
  async create(
    @Args('createRoomInput', { type: () => CreateRoomInput })
    createRoomInput: CreateRoomInput,
    @Context()
    { req },
  ) {
    return this.roomsService.create(req.user.id, createRoomInput.userIds);
  }

  // TODO: Need another response object
  @Mutation(() => Room, { name: 'readRoomMessages' })
  async read(
    @Args('roomId', { type: () => String }) roomId: string,
    @Context()
    { req },
  ) {
    return this.roomsService.read(req.user.id, roomId);
  }

  @Mutation(() => String)
  async startDialog(
    @Args('userId', { type: () => String })
    userId: string,
    @Context()
    { req },
  ) {
    return this.roomsService.startDialog(req.user.id, userId);
  }

  @Mutation(() => String, { name: 'leaveRoom' })
  async leave(
    @Args('roomId', { type: () => String }) roomId: string,
    @Context()
    { req },
  ) {
    return this.roomsService.leave(req.user.id, roomId);
  }
}
