import { Field, ID, InputType } from '@nestjs/graphql';

@InputType()
export class CreateRoomInput {
  @Field(() => [ID])
  readonly userIds: string[];
}
