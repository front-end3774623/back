import { Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';
import { Message } from '../../messages/entities/message.entity';
import { Call } from '../../calls/entities/call.entity';
import { RoomUser } from './room-user.entity';
import { User } from '../../users/entities/user.entity';

@Entity({ name: 'rooms' })
@ObjectType('Room')
export class Room {
  @PrimaryGeneratedColumn('uuid')
  @Field()
  id: string;

  @Field(() => [Message])
  @OneToMany(() => Message, (message) => message.room, {
    onDelete: 'CASCADE',
    cascade: true,
  })
  messages: Message[];

  @Field(() => [Call])
  @OneToMany(() => Call, (call) => call.room, {
    onDelete: 'CASCADE',
    cascade: true,
  })
  calls: Call[];

  @OneToMany(() => RoomUser, (roomUser) => roomUser.room)
  roomsUsers: RoomUser[];

  @Field(() => [User])
  get members() {
    return this.roomsUsers.map((roomUser) => roomUser.user);
  }
}
