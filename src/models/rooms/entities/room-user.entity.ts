import {
  Column,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';
import { Room } from './room.entity';
import { User } from '../../users/entities/user.entity';

@Entity({ name: 'room_user' })
@ObjectType('RoomUser')
export class RoomUser {
  @PrimaryGeneratedColumn('uuid')
  @Field()
  id: string;

  @DeleteDateColumn()
  @Field()
  deletedAt?: Date;

  @Column()
  roomId: string;

  @Column()
  userId: string;

  @ManyToOne(() => Room, (room) => room.roomsUsers)
  room: Room;

  @ManyToOne(() => User, (user) => user.roomsUsers)
  user: User;
}
