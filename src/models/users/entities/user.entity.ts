import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { Field, ID, Int, ObjectType } from '@nestjs/graphql';
import { Session } from '../../sessions/entities/session.entity';
import { Auth } from '../../../auth/entities/auth.entity';
import { UserSex } from '../../../common/types/enums';
import { RoomUser } from '../../rooms/entities/room-user.entity';

@Entity({ name: 'users' })
@ObjectType('User')
export class User {
  @PrimaryGeneratedColumn('uuid')
  @Field(() => ID)
  id: string;

  @Column('varchar', {
    length: 100,
    nullable: false,
  })
  @Field()
  firstName: string;

  @Column('varchar', {
    length: 100,
    nullable: false,
  })
  @Field()
  lastName: string;

  @Column('date')
  @Field()
  dateOfBirth: string;

  @Column()
  @Field(() => Int)
  nationality: number;

  @Column({ type: 'enum', enum: UserSex })
  @Field()
  sex: UserSex;

  @Column('boolean', { default: true })
  @Field(() => Boolean)
  isActive: boolean;

  @CreateDateColumn()
  @Field()
  created_at: Date;

  @UpdateDateColumn()
  @Field()
  updated_at: Date;

  @Column('boolean', { default: false })
  @Field(() => Boolean)
  isOnline: boolean;

  @OneToOne(() => Auth, (auth) => auth.user, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'auth_id' })
  auth: Auth;

  @OneToMany(() => Session, (session) => session.user, { onDelete: 'CASCADE' })
  sessions: Session[];

  @OneToMany(() => RoomUser, (roomUser) => roomUser.user)
  roomsUsers: RoomUser[];

  @Field(() => [User])
  get rooms() {
    return this.roomsUsers.map((roomUser) => roomUser.room);
  }
}
