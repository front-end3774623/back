import { Args, Context, Query, Resolver } from '@nestjs/graphql';
import { UsersService } from './users.service';
import { User } from './entities/user.entity';

@Resolver()
export class UsersResolver {
  constructor(private usersService: UsersService) {}

  @Query(() => User, { name: 'user' })
  async find(@Args('userId', { type: () => String }) userId: string) {
    return this.usersService.findById(userId);
  }

  @Query(() => [User], { name: 'users' })
  async findAll(
    @Context()
    { req },
  ) {
    return this.usersService.findAll(req.user.id);
  }
}
