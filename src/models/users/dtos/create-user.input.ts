import { Field, InputType } from '@nestjs/graphql';
import { IsDateString, IsEnum, IsInt, Length } from 'class-validator';
import { UserSex } from '../../../common/types/enums';

@InputType()
export class CreateUserInput {
  @Field()
  @Length(2, 50)
  firstName: string;

  @Field()
  @Length(2, 50)
  lastName: string;

  @Field()
  @IsDateString()
  dateOfBirth: string;

  @Field()
  @IsInt()
  nationality: number;

  @Field()
  @IsEnum(UserSex)
  sex: UserSex;
}
