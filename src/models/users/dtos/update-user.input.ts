import { Field, InputType } from '@nestjs/graphql';
import {
  IsBoolean,
  IsDateString,
  IsEnum,
  IsInt,
  IsOptional,
  IsUUID,
  Length,
} from 'class-validator';
import { UserSex } from '../../../common/types/enums';

@InputType()
export class UpdateUserInput {
  @Field({ nullable: false })
  @IsUUID()
  id: string;

  @Field()
  @IsOptional()
  @Length(2, 50)
  firstName?: string;

  @Field()
  @IsOptional()
  @Length(2, 50)
  lastName?: string;

  @Field()
  @IsOptional()
  @IsDateString()
  dateOfBirth?: string;

  @Field()
  @IsOptional()
  @IsInt()
  nationality?: number;

  @Field()
  @IsOptional()
  @IsEnum(UserSex)
  sex?: UserSex;

  @Field()
  @IsOptional()
  @IsBoolean()
  isActive?: boolean;

  @Field()
  @IsOptional()
  @IsBoolean()
  isOnline?: boolean;
}
