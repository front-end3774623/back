import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { In, Not, Repository } from 'typeorm';
import { CreateUserInput } from './dtos/create-user.input';
import { Auth } from '../../auth/entities/auth.entity';
import { UpdateUserInput } from './dtos/update-user.input';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async findById(id: string) {
    return this.usersRepository.findOne({ where: { id } });
  }

  async findAll(userId: string) {
    return this.usersRepository.find({ where: { id: Not(userId) } });
  }

  async findByIds(ids: string[]) {
    return this.usersRepository.findBy({ id: In(ids) });
  }

  async create(createUserInput: CreateUserInput, auth: Auth) {
    const user = this.usersRepository.create(createUserInput);

    user.auth = auth;

    return this.usersRepository.save(user);
  }

  async update(user: UpdateUserInput) {
    return this.usersRepository.update(user.id, user);
  }
}
