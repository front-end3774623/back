import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Session } from './entities/session.entity';
import { Equal, Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { User } from '../users/entities/user.entity';

@Injectable()
export class SessionsService {
  constructor(
    @InjectRepository(Session)
    private sessionRepository: Repository<Session>,
    private jwtService: JwtService,
  ) {}

  async create(user: User) {
    const refreshToken = this.jwtService.sign({
      sub: user.id,
    });

    const session = this.sessionRepository.create({
      value: refreshToken,
    });

    await this.sessionRepository.save({
      ...session,
      user,
    });

    return refreshToken;
  }

  async getUserByRefreshToken(refreshToken: string) {
    const session = await this.sessionRepository.findOne({
      where: { value: Equal(refreshToken) },
      relations: ['user', 'user.auth'],
    });

    if (!session) {
      throw new UnauthorizedException();
    }

    if (session.isUsed /* refresh token was stolen */) {
      await this.sessionRepository.delete({
        user: { id: Equal(session.user.id) },
      });

      throw new UnauthorizedException();
    }

    await this.markRefreshTokenAsUsed(refreshToken);

    return session.user;
  }

  async markRefreshTokenAsUsed(refreshToken: string) {
    await this.sessionRepository.update(
      { value: Equal(refreshToken) },
      { isUsed: true },
    );
  }
}
