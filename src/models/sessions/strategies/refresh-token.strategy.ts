import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-jwt';
import { FastifyRequest } from 'fastify';

@Injectable()
export class RefreshTokenStrategy extends PassportStrategy(
  Strategy,
  'jwt-refresh',
) {
  constructor() {
    super({
      jwtFromRequest: RefreshTokenStrategy.fromHttpOnlyCookie,
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_REFRESH_SECRET,
    });
  }

  async validate(payload: any) {
    return payload;
  }

  private static fromHttpOnlyCookie(req: FastifyRequest) {
    const refreshToken = req.cookies['_my_session'];

    return refreshToken || null;
  }
}
