import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { User } from '../../users/entities/user.entity';

@Entity({ name: 'sessions' })
@ObjectType('Session')
export class Session {
  @PrimaryGeneratedColumn('uuid')
  @Field(() => ID)
  id: string;

  @Column()
  @Field()
  value: string;

  @Column('boolean', { default: false })
  @Field(() => Boolean)
  isUsed: boolean;

  @ManyToOne(() => User, (user) => user.sessions, { eager: true })
  user: User;
}
