import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SessionsService } from './sessions.service';
import { Session } from './entities/session.entity';
import { RefreshTokenStrategy } from './strategies/refresh-token.strategy';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    TypeOrmModule.forFeature([Session]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get('JWT_REFRESH_SECRET'),
        signOptions: {
          expiresIn: parseInt(
            configService.get('JWT_REFRESH_TOKEN_VALIDITY_DURATION_IN_SEC'),
          ),
        },
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [SessionsService, RefreshTokenStrategy],
  exports: [SessionsService],
})
export class SessionsModule {}
