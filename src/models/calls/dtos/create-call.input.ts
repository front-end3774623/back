import { Field, ID, InputType } from '@nestjs/graphql';

@InputType()
export class CreateCallInput {
  @Field(() => ID)
  readonly roomId: string;

  @Field(() => [ID])
  readonly userIds: string[];
}
