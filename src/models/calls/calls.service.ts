import { Inject, Injectable } from '@nestjs/common';
import { PUB_SUB } from '../../common/constants/injection-token';
import { PubSub } from 'graphql-subscriptions';
import { InjectRepository } from '@nestjs/typeorm';
import { Call } from './entities/call.entity';
import { Repository } from 'typeorm';
import { CreateCallInput } from './dtos/create-call.input';
import { User } from '../users/entities/user.entity';
import { RoomsService } from '../rooms/rooms.service';
import { CALL_STARTED } from './constants';
import { InvitedUser } from './entities/invited-user.entity';

@Injectable()
export class CallsService {
  constructor(
    @InjectRepository(Call)
    private callRepository: Repository<Call>,
    @InjectRepository(InvitedUser)
    private invitedUserRepository: Repository<InvitedUser>,
    private roomsService: RoomsService,
    @Inject(PUB_SUB) private readonly pubSub: PubSub,
  ) {}

  async findById(callId: string) {
    return this.callRepository.findOne({
      where: { id: callId },
      relations: {
        author: true,
        participants: true,
      },
    });
  }

  async create(author: User, createCallInput: CreateCallInput) {
    const call = this.callRepository.create({
      author,
    });

    const room = await this.roomsService.findById(
      author.id,
      createCallInput.roomId,
    );
    call.room = room;

    const updatedCall = await this.callRepository.save(call);

    await this.invitedUserRepository.insert(
      createCallInput.userIds.map((userId) => ({
        callId: call.id,
        userId,
      })),
    );

    await this.pubSub.publish(CALL_STARTED, {
      callStarted: {
        ...updatedCall,
        usersId: createCallInput.userIds,
      },
    });

    return updatedCall;
  }

  async acceptCall(userId: string, callId: string) {
    const call = await this.findById(callId);

    const invitedUser = await this.invitedUserRepository.findOne({
      where: { callId, userId },
      relations: {
        user: true,
      },
    });

    invitedUser.isInvitationAccepted = true;

    call.participants = call.participants
      ? [...call.participants, invitedUser.user]
      : [invitedUser.user];

    await this.invitedUserRepository.save(invitedUser);

    return this.callRepository.save(call);
  }

  async finishCall(userId: string, callId: string) {
    const call = await this.findById(callId);

    call.participants = call.participants.filter(
      (participant) => participant.id !== userId,
    );

    return this.callRepository.save(call);
  }
}
