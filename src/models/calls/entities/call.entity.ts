import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';
import { Room } from '../../rooms/entities/room.entity';
import { User } from '../../users/entities/user.entity';

@Entity({ name: 'calls' })
@ObjectType('Call')
export class Call {
  @PrimaryGeneratedColumn('uuid')
  @Field()
  id: string;

  @Field(() => User)
  @ManyToOne(() => User, (user) => user)
  @JoinColumn({ name: 'author_id' })
  author: User;

  @CreateDateColumn()
  @Field()
  created_at: Date;

  @ManyToOne(() => Room, (room) => room.calls)
  @Field(() => Room)
  room: Room;

  @Column('boolean', { default: true })
  @Field(() => Boolean)
  isActive: boolean;

  @Field(() => [User], { nullable: true })
  @ManyToMany(() => User)
  @JoinTable({
    name: 'calls_participants',
  })
  participants: User[];
}
