import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { User } from '../../users/entities/user.entity';
import { Call } from './call.entity';

@Entity({ name: 'invited_users' })
@ObjectType('InvitedUser')
export class InvitedUser {
  @Column('boolean', { default: false })
  @Field(() => Boolean)
  isInvitationAccepted: boolean;

  @PrimaryColumn('uuid')
  @Field(() => ID)
  userId: string;

  @PrimaryColumn('uuid')
  @Field(() => ID)
  callId: string;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'userId' })
  user: User;

  @ManyToOne(() => Call)
  @JoinColumn({ name: 'callId' })
  call: Call;
}
