import {
  Args,
  Context,
  Mutation,
  Resolver,
  Subscription,
} from '@nestjs/graphql';
import { Inject } from '@nestjs/common';
import { PubSub } from 'graphql-subscriptions';
import { PUB_SUB } from '../../common/constants/injection-token';
import { CallsService } from './calls.service';
import { Call } from './entities/call.entity';
import { CreateCallInput } from './dtos/create-call.input';
import { CALL_STARTED } from './constants';

@Resolver()
export class CallsResolver {
  constructor(
    private callsService: CallsService,
    @Inject(PUB_SUB) private readonly pubSub: PubSub,
  ) {}

  @Mutation(() => Call, { name: 'call' })
  async create(
    @Args('createCallInput', { type: () => CreateCallInput })
    createCallInput: CreateCallInput,
    @Context()
    { req },
  ) {
    return this.callsService.create(req.user, createCallInput);
  }

  @Subscription(() => Call, {
    filter: (call, _, { req }) => {
      const usersId: string[] = call.callStarted.usersId;

      return !!usersId.find((userId) => userId === req.user.id);
    },
  })
  callStarted() {
    return this.pubSub.asyncIterator(CALL_STARTED);
  }

  @Mutation(() => Call, { name: 'acceptCall' })
  async acceptCall(
    @Args('callId', { type: () => String }) callId: string,
    @Context()
    { req },
  ) {
    return this.callsService.acceptCall(req.user.id, callId);
  }

  @Mutation(() => Call, { name: 'finishCall' })
  async finishCall(
    @Args('callId', { type: () => String }) callId: string,
    @Context()
    { req },
  ) {
    return this.callsService.finishCall(req.user.id, callId);
  }
}
