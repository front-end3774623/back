import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Call } from './entities/call.entity';
import { CallsResolver } from './calls.resolver';
import { CallsService } from './calls.service';
import { RoomsModule } from '../rooms/rooms.module';
import { InvitedUser } from './entities/invited-user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Call, InvitedUser]), RoomsModule],
  providers: [CallsResolver, CallsService],
})
export class CallsModule {}
