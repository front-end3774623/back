export enum UserSex {
  MALE = 'male',
  FEMALE = 'female',
  OTHER = 'other',
}
