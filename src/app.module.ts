import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { DatabaseModule } from './config/database/database.module';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { join } from 'path';
import { FastifyReply, FastifyRequest } from 'fastify';
import { PubSubModule } from './common/pubsub/pubsub.module';
import { ApolloServerPluginLandingPageLocalDefault } from '@apollo/server/plugin/landingPage/default';
import { MessagesModule } from './models/messages/messages.module';
import { UsersModule } from './models/users/users.module';
import { RoomsModule } from './models/rooms/rooms.module';
import { CallsModule } from './models/calls/calls.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: process.env.NODE_ENV === 'dev' ? `.env.dev` : '.env.prod',
    }),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: join(process.cwd(), 'schema.gql'),
      sortSchema: true,
      context: (req: FastifyRequest, reply: FastifyReply) => ({
        req,
        reply,
      }),
      playground: false,
      plugins: [
        ApolloServerPluginLandingPageLocalDefault({
          embed: { endpointIsEditable: true },
        }),
      ],
      subscriptions: {
        'graphql-ws': {
          path: '/subscriptions',
        },
      },
    }),
    DatabaseModule,
    AuthModule,
    UsersModule,
    CallsModule,
    RoomsModule,
    MessagesModule,
    PubSubModule,
  ],
})
export class AppModule {}
