import { MigrationInterface, QueryRunner } from 'typeorm';

export class RenameRoomMembersTable1740069470773 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE room_members RENAME COLUMN "roomsId" TO "roomId"',
    );
    await queryRunner.query(
      'ALTER TABLE room_members RENAME COLUMN "usersId" TO "userId"',
    );
    await queryRunner.query('ALTER TABLE room_members RENAME TO room_user');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE room_user RENAME COLUMN "roomId" TO "roomsId"',
    );
    await queryRunner.query(
      'ALTER TABLE room_user RENAME COLUMN "userId" TO "usersId"',
    );
    await queryRunner.query('ALTER TABLE room_user RENAME TO room_members');
  }
}
