import { MigrationInterface, QueryRunner } from "typeorm";

export class AddUnreadMessageDetection1734094172768 implements MigrationInterface {
    name = 'AddUnreadMessageDetection1734094172768'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "messages_users" ("messagesId" uuid NOT NULL, "usersId" uuid NOT NULL, CONSTRAINT "PK_c27ba84c107f94634d8ff06635c" PRIMARY KEY ("messagesId", "usersId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_bd7a40f33fb9e28657b5d9f25a" ON "messages_users" ("messagesId") `);
        await queryRunner.query(`CREATE INDEX "IDX_89de576157903f97289f42b883" ON "messages_users" ("usersId") `);
        await queryRunner.query(`ALTER TABLE "messages_users" ADD CONSTRAINT "FK_bd7a40f33fb9e28657b5d9f25a2" FOREIGN KEY ("messagesId") REFERENCES "messages"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "messages_users" ADD CONSTRAINT "FK_89de576157903f97289f42b8837" FOREIGN KEY ("usersId") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "messages_users" DROP CONSTRAINT "FK_89de576157903f97289f42b8837"`);
        await queryRunner.query(`ALTER TABLE "messages_users" DROP CONSTRAINT "FK_bd7a40f33fb9e28657b5d9f25a2"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_89de576157903f97289f42b883"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_bd7a40f33fb9e28657b5d9f25a"`);
        await queryRunner.query(`DROP TABLE "messages_users"`);
    }

}
