import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateRoomUserTable1740069630309 implements MigrationInterface {
    name = 'UpdateRoomUserTable1740069630309'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "room_user" DROP CONSTRAINT "FK_a0a48bec4d18dbc8f39356aea44"`);
        await queryRunner.query(`ALTER TABLE "room_user" DROP CONSTRAINT "FK_ab3822e605d62a61f1ea71e44e9"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_ab3822e605d62a61f1ea71e44e"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_a0a48bec4d18dbc8f39356aea4"`);
        await queryRunner.query(`ALTER TABLE "room_user" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`);
        await queryRunner.query(`ALTER TABLE "room_user" DROP CONSTRAINT "PK_72f96df50d5a047f75fe0c91755"`);
        await queryRunner.query(`ALTER TABLE "room_user" ADD CONSTRAINT "PK_72f96df50d5a047f75fe0c91755" PRIMARY KEY ("userId", "roomId", "id")`);
        await queryRunner.query(`ALTER TABLE "room_user" ADD "deletedAt" TIMESTAMP`);
        await queryRunner.query(`ALTER TABLE "room_user" DROP CONSTRAINT "PK_72f96df50d5a047f75fe0c91755"`);
        await queryRunner.query(`ALTER TABLE "room_user" ADD CONSTRAINT "PK_72f96df50d5a047f75fe0c91755" PRIMARY KEY ("userId", "id")`);
        await queryRunner.query(`ALTER TABLE "room_user" DROP CONSTRAINT "PK_72f96df50d5a047f75fe0c91755"`);
        await queryRunner.query(`ALTER TABLE "room_user" ADD CONSTRAINT "PK_4bae79e46b7d9395a7ebdf86423" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "room_user" ADD CONSTRAINT "FK_507b03999779b22e06538595dec" FOREIGN KEY ("roomId") REFERENCES "rooms"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "room_user" ADD CONSTRAINT "FK_27dad61266db057665ee1b13d3d" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "room_user" DROP CONSTRAINT "FK_27dad61266db057665ee1b13d3d"`);
        await queryRunner.query(`ALTER TABLE "room_user" DROP CONSTRAINT "FK_507b03999779b22e06538595dec"`);
        await queryRunner.query(`ALTER TABLE "room_user" DROP CONSTRAINT "PK_4bae79e46b7d9395a7ebdf86423"`);
        await queryRunner.query(`ALTER TABLE "room_user" ADD CONSTRAINT "PK_72f96df50d5a047f75fe0c91755" PRIMARY KEY ("userId", "id")`);
        await queryRunner.query(`ALTER TABLE "room_user" DROP CONSTRAINT "PK_72f96df50d5a047f75fe0c91755"`);
        await queryRunner.query(`ALTER TABLE "room_user" ADD CONSTRAINT "PK_72f96df50d5a047f75fe0c91755" PRIMARY KEY ("userId", "roomId", "id")`);
        await queryRunner.query(`ALTER TABLE "room_user" DROP COLUMN "deletedAt"`);
        await queryRunner.query(`ALTER TABLE "room_user" DROP CONSTRAINT "PK_72f96df50d5a047f75fe0c91755"`);
        await queryRunner.query(`ALTER TABLE "room_user" ADD CONSTRAINT "PK_72f96df50d5a047f75fe0c91755" PRIMARY KEY ("userId", "roomId")`);
        await queryRunner.query(`ALTER TABLE "room_user" DROP COLUMN "id"`);
        await queryRunner.query(`CREATE INDEX "IDX_a0a48bec4d18dbc8f39356aea4" ON "room_user" ("roomId") `);
        await queryRunner.query(`CREATE INDEX "IDX_ab3822e605d62a61f1ea71e44e" ON "room_user" ("userId") `);
        await queryRunner.query(`ALTER TABLE "room_user" ADD CONSTRAINT "FK_ab3822e605d62a61f1ea71e44e9" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "room_user" ADD CONSTRAINT "FK_a0a48bec4d18dbc8f39356aea44" FOREIGN KEY ("roomId") REFERENCES "rooms"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
