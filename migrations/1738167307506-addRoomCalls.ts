import { MigrationInterface, QueryRunner } from "typeorm";

export class AddRoomCalls1738167307506 implements MigrationInterface {
    name = 'AddRoomCalls1738167307506'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "calls" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "isActive" boolean NOT NULL DEFAULT true, "author_id" uuid, "roomId" uuid, CONSTRAINT "PK_d9171d91f8dd1a649659f1b6a20" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "invited_users" ("isInvitationAccepted" boolean NOT NULL DEFAULT false, "userId" uuid NOT NULL, "callId" uuid NOT NULL, CONSTRAINT "PK_67da846843be1e3e7c4c1375492" PRIMARY KEY ("userId", "callId"))`);
        await queryRunner.query(`CREATE TABLE "calls_participants" ("callsId" uuid NOT NULL, "usersId" uuid NOT NULL, CONSTRAINT "PK_5adb66f519db0c6ac94691d88c0" PRIMARY KEY ("callsId", "usersId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_6635f2bd420b28811824e30f5a" ON "calls_participants" ("callsId") `);
        await queryRunner.query(`CREATE INDEX "IDX_b1392c2df18295e8975bc3f6ab" ON "calls_participants" ("usersId") `);
        await queryRunner.query(`ALTER TABLE "calls" ADD CONSTRAINT "FK_f907a44b9e25cac4f32203070ec" FOREIGN KEY ("author_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "calls" ADD CONSTRAINT "FK_a4debb153d08c1a453a8439995e" FOREIGN KEY ("roomId") REFERENCES "rooms"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "invited_users" ADD CONSTRAINT "FK_ef9e5f9840818dbe2e91d0d599c" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "invited_users" ADD CONSTRAINT "FK_f7d8862b67370edf70a6671ba1e" FOREIGN KEY ("callId") REFERENCES "calls"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "calls_participants" ADD CONSTRAINT "FK_6635f2bd420b28811824e30f5a0" FOREIGN KEY ("callsId") REFERENCES "calls"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "calls_participants" ADD CONSTRAINT "FK_b1392c2df18295e8975bc3f6ab4" FOREIGN KEY ("usersId") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "calls_participants" DROP CONSTRAINT "FK_b1392c2df18295e8975bc3f6ab4"`);
        await queryRunner.query(`ALTER TABLE "calls_participants" DROP CONSTRAINT "FK_6635f2bd420b28811824e30f5a0"`);
        await queryRunner.query(`ALTER TABLE "invited_users" DROP CONSTRAINT "FK_f7d8862b67370edf70a6671ba1e"`);
        await queryRunner.query(`ALTER TABLE "invited_users" DROP CONSTRAINT "FK_ef9e5f9840818dbe2e91d0d599c"`);
        await queryRunner.query(`ALTER TABLE "calls" DROP CONSTRAINT "FK_a4debb153d08c1a453a8439995e"`);
        await queryRunner.query(`ALTER TABLE "calls" DROP CONSTRAINT "FK_f907a44b9e25cac4f32203070ec"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_b1392c2df18295e8975bc3f6ab"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_6635f2bd420b28811824e30f5a"`);
        await queryRunner.query(`DROP TABLE "calls_participants"`);
        await queryRunner.query(`DROP TABLE "invited_users"`);
        await queryRunner.query(`DROP TABLE "calls"`);
    }

}
