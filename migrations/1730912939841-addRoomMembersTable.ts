import { MigrationInterface, QueryRunner } from "typeorm";

export class AddRoomMembersTable1730912939841 implements MigrationInterface {
    name = 'AddRoomMembersTable1730912939841'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "rooms" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), CONSTRAINT "PK_0368a2d7c215f2d0458a54933f2" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "room_members" ("usersId" uuid NOT NULL, "roomsId" uuid NOT NULL, CONSTRAINT "PK_72f96df50d5a047f75fe0c91755" PRIMARY KEY ("usersId", "roomsId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_ab3822e605d62a61f1ea71e44e" ON "room_members" ("usersId") `);
        await queryRunner.query(`CREATE INDEX "IDX_a0a48bec4d18dbc8f39356aea4" ON "room_members" ("roomsId") `);
        await queryRunner.query(`ALTER TABLE "room_members" ADD CONSTRAINT "FK_ab3822e605d62a61f1ea71e44e9" FOREIGN KEY ("usersId") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "room_members" ADD CONSTRAINT "FK_a0a48bec4d18dbc8f39356aea44" FOREIGN KEY ("roomsId") REFERENCES "rooms"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "room_members" DROP CONSTRAINT "FK_a0a48bec4d18dbc8f39356aea44"`);
        await queryRunner.query(`ALTER TABLE "room_members" DROP CONSTRAINT "FK_ab3822e605d62a61f1ea71e44e9"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_a0a48bec4d18dbc8f39356aea4"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_ab3822e605d62a61f1ea71e44e"`);
        await queryRunner.query(`DROP TABLE "room_members"`);
        await queryRunner.query(`DROP TABLE "rooms"`);
    }

}
